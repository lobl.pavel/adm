#include "nn.h"
#include <math.h>
#include <stdio.h>

#define WEIGHT_RND_MIN -0.2
#define WEIGHT_RND_MAX  0.2

#define LEARNING_RATE   (0.2)

static float sigmoid(float x) {
	return 1.0 / (1 + powf(M_E, x));
}

/* static float derivative_sigmoid(float x) {
	return sigmoid(x)*(1 - sigmoid(x));
} */

void net_init(struct net *n, int input_size, int hidden_size, int output_size) {

	mat_init(&n->in.val, input_size, 1);
	mat_init(&n->in.val_trans, 1, input_size);

	mat_init(&n->hid.weight, hidden_size, input_size);
	mat_rand(&n->hid.weight, WEIGHT_RND_MIN, WEIGHT_RND_MAX);

	mat_init(&n->hid.val, hidden_size, 1);
	mat_init(&n->hid.val_trans, 1, hidden_size);
	mat_init(&n->hid.delta, hidden_size, 1);
	mat_init(&n->hid.delta_weight, hidden_size, input_size);
	mat_init(&n->hid.bias, hidden_size, 1);
	mat_rand(&n->hid.bias, WEIGHT_RND_MIN, WEIGHT_RND_MAX);
	mat_init(&n->hid.delta_bias, hidden_size, 1);

	/* initialize output layer */
	mat_init(&n->out.weight, output_size, hidden_size);
	mat_rand(&n->out.weight, WEIGHT_RND_MIN, WEIGHT_RND_MAX);

	mat_init(&n->out.val, output_size, 1);
	mat_init(&n->out.bias, output_size, 1);
	mat_rand(&n->out.bias, WEIGHT_RND_MIN, WEIGHT_RND_MAX);

	mat_init(&n->out.delta, output_size, 1);
	mat_init(&n->out.delta_bias, output_size, 1);
	mat_init(&n->out.delta_weight, output_size, hidden_size);

}

void net_backprop(struct net *n, struct mat *target) {
	/* calculate output layer deltas */
	int i;
	for (i = 0; i < n->out.val.rows; i++) {
		float out = mat_elem(&n->out.val, i, 0);
		float target_value = mat_elem(target, i, 0);
		float delta = out*(1 - out)*(out - target_value);
		printf("out: %f target: %f diff: %f del: %f\n", out, target_value, out - target_value, delta);
		mat_set_elem(&n->out.delta, i, 0, delta);
	}
	printf("OUT DELTA\n");
	mat_print(&n->out.delta);

	/* calculate second deltas for the output layer */
	mat_mul(&n->out.delta, &n->hid.val_trans, &n->out.delta_weight);
	printf("deltas 2\n");
	mat_scalar(&n->out.delta_weight, -LEARNING_RATE, &n->out.delta_weight);
	mat_print(&n->out.delta_weight);

	printf("deltas bias\n");
	mat_scalar(&n->out.delta, -LEARNING_RATE, &n->out.delta_bias);
	mat_print(&n->out.delta_bias);

	for (i = 0; i < n->hid.val.rows; i++) {
		float out = mat_elem(&n->hid.val, i, 0);
		float sum = 0;
		int j;
		for (j = 0; j < n->out.delta.rows; j++) {
			float delta = mat_elem(&n->out.delta, j, 0);
			float weight = mat_elem(&n->out.weight, j, i);
			printf("delta: %f weight: %f\n", delta, weight);
			sum += weight*delta;
		}
		printf("sum: %f\n", sum);

		mat_set_elem(&n->hid.delta, i, 0, out*(1 - out)*sum);
	}
	printf("hid delta\n");
	mat_print(&n->hid.delta);
	mat_mul(&n->hid.delta, &n->in.val_trans, &n->hid.delta_weight);
	mat_scalar(&n->hid.delta_weight, -LEARNING_RATE, &n->hid.delta_weight);

	mat_scalar(&n->hid.delta, -LEARNING_RATE, &n->hid.delta_bias);

	/* finally */
	printf("update weights\n");
	mat_print(&n->out.weight);
	mat_print(&n->hid.weight);
	mat_print(&n->hid.delta_weight);
	mat_add(&n->out.weight, &n->out.delta_weight, &n->out.weight);
	mat_add(&n->hid.weight, &n->hid.delta_weight, &n->hid.weight);

	printf("\n");
	mat_print(&n->out.weight);
	mat_print(&n->hid.weight);

	printf("update biases\n");
	mat_print(&n->out.bias);
	mat_print(&n->hid.bias);
	mat_add(&n->hid.bias, &n->hid.delta_bias, &n->hid.bias);
	mat_add(&n->out.bias, &n->out.delta_bias, &n->out.bias);
	printf("\n");
	mat_print(&n->out.weight);
	mat_print(&n->out.bias);
	mat_print(&n->hid.bias);
};

const struct mat* net_propagate(struct net *n, const struct mat *input) {
	mat_copy(input, &n->in.val);

	mat_trans(&n->in.val, &n->in.val_trans);
#ifdef _DEBUG_NET
	printf("input\n");
	mat_print(&n->in.val);
#endif
	mat_mul(&n->hid.weight, &n->in.val, &n->hid.val);
	mat_add(&n->hid.val, &n->hid.bias, &n->hid.val);
	mat_each(&n->hid.val, sigmoid, &n->hid.val);
	mat_trans(&n->hid.val, &n->hid.val_trans);
#ifdef _DEBUG_NET
	printf("hid out\n");
	mat_print(&n->hid.val);
#endif
	mat_mul(&n->out.weight, &n->hid.val, &n->out.val);
	mat_add(&n->out.val, &n->out.bias, &n->out.val);
	mat_each(&n->out.val, sigmoid, &n->out.val);
#ifdef _DEBUG_NET
	printf("out out\n");
	mat_print(&n->out.val);
#endif
	return &n->out.val;
}
