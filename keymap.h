#ifndef KEYMAP_H
#define KEYMAP_H

#include <stdio.h>

struct key_map {
	char key_start;
	char key_end;
	int size;
	struct mat *targets;
};

void key_map_init(struct key_map *map, int size, char start);
const struct mat* key_map_target(struct key_map *map, char key);
char key_map_get_first(struct key_map *map);
char key_map_get_last(struct key_map *map);
void key_map_free(struct key_map *map);

#endif
