#include "dataset.h"
#include <stdlib.h>

void data_set_free(struct data_set *data) {
	free(data->input);
	free(data->output);
}

void data_set_init(struct data_set *data, unsigned input, unsigned output) {
	int i;
	data->allocated_samples = 1; /* TODO: for testing */
	data->input = (struct mat*)malloc(data->allocated_samples*sizeof(struct mat));
	data->output = (struct mat*)malloc(data->allocated_samples*sizeof(struct mat));
	data->num_samples = 0;
	data->input_size = input;
	data->output_size = output;
	for (i = 0; i < data->allocated_samples; i++)
		mat_init(&data->input[i], input, 1);
	for (i = 0; i < data->allocated_samples; i++)
		mat_init(&data->output[i], output, 1);
}

void data_set_add(struct data_set *data, const struct mat *in, const struct mat *out) {
	int samples = data->num_samples;
	if (data->allocated_samples == data->num_samples) {
		int i;
		int initialized = data->allocated_samples;
		data->allocated_samples *= 2;
		data->input = (struct mat*)realloc(data->input,
							    		  data->allocated_samples*sizeof(struct mat));
		data->output = (struct mat*)realloc(data->output,
							    		  data->allocated_samples*sizeof(struct mat));
		for (i = initialized; i < data->allocated_samples; i++)
			mat_init(&data->output[i], data->output_size, 1);
		for (i = initialized; i < data->allocated_samples; i++)
			mat_init(&data->input[i], data->input_size, 1);
	}
	mat_copy(in, &data->input[samples]);
	mat_copy(out, &data->output[samples]);
	data->num_samples++;
}

void data_set_for_each(struct data_set *data,
					   void (*cb)(const struct mat *in, const struct mat *out, void *user),
					   void *user) {
	int i;
	for (i = 0; i < data->num_samples; i++) {
		cb(&data->input[i], &data->output[i], user);
	}
}

void data_set_read(struct data_set *data, FILE *in_file) {
	uint32_t *buffer = (uint32_t*)malloc(sizeof(uint32_t)*3);
	size_t ret = fread(buffer, sizeof(uint32_t), 3, in_file);
	if (ret < 3) {
		perror("fread");
		exit(EXIT_FAILURE);
	}
	data->input_size = buffer[0];
	data->output_size = buffer[1];
	data->num_samples = buffer[2];
	data->allocated_samples = data->num_samples;
	if (data->allocated_samples == 0)
		data->allocated_samples = 1;

	data->input = (struct mat*)malloc(sizeof(struct mat)*data->allocated_samples);
	data->output = (struct mat*)malloc(sizeof(struct mat)*data->allocated_samples);

	free(buffer);

	int samples_size = data->num_samples*(data->input_size+data->output_size);
	float *samples = (float*)malloc(sizeof(float)*samples_size);
	ret = fread(samples, sizeof(float), samples_size, in_file);
	if (ret < data->num_samples) {
		if (feof(in_file))
			fprintf(stderr, "unexpected end of file, malformed header? exiting\n");
		else
			perror("fread");
		exit(EXIT_FAILURE);
	}

	unsigned i, j;
	for (i = 0; i < data->allocated_samples; i++) {
		mat_init(&data->input[i], data->input_size, 1);
		mat_init(&data->output[i], data->output_size, 1);
	}

	float *p = samples;
	for (i = 0; i < data->num_samples; i++) {
		for (j = 0; j < data->input_size; j++)
			mat_set_elem(&data->input[i], j, 0, *p++);
		for (j = 0; j < data->output_size; j++)
			mat_set_elem(&data->output[i], j, 0, *p++);
	}
	free(samples);
#if _DEBUG
	printf("dataset loaded: %d samples, in %d, out %d\n",
			data->num_samples, data->input_size, data->output_size);
#endif
	return;
}

void data_set_print(struct data_set *data) {
	int i;
	for (i = 0; i < data->num_samples; i++) {
		printf("%d IN:\n", i);
		mat_print(&data->input[i]);
		printf("%d OUT:\n", i);
		mat_print(&data->output[i]);
	}
}

void data_set_write(struct data_set *data, FILE *out_file) {
	uint32_t buffer[3];
	buffer[0] = data->input_size;
	buffer[1] = data->output_size;
	buffer[2] = data->num_samples;
	size_t ret = fwrite(buffer, sizeof(uint32_t), 3, out_file);
	if (ret < 3) {
		perror("fwrite");
		exit(EXIT_FAILURE);
	}
	int samples_size = data->num_samples*(data->input_size+data->output_size);
	float *samples = (float*)malloc(sizeof(float)*samples_size);

	float *p = samples;
	unsigned i, j;
	for (i = 0; i < data->num_samples; i++) {
		for (j = 0; j < data->input_size; j++)
			*p++ = mat_elem(&data->input[i], j, 0);
		for (j = 0; j < data->output_size; j++)
			*p++ = mat_elem(&data->output[i], j, 0);
	}
	ret = fwrite(samples, sizeof(float), samples_size, out_file);
	if (ret < samples_size) {
		perror("fwrite");
		exit(EXIT_FAILURE);
	}
}
