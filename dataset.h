#ifndef DATASET_H
#define DATASET_H
#include <stdio.h>
#include <stdint.h>
#include "mat.h"

struct data_set {
	uint32_t input_size;
	uint32_t output_size;
	uint32_t num_samples;
	uint32_t allocated_samples;
	struct mat *input;
	struct mat *output;
};

void data_set_free(struct data_set *data);
void data_set_for_each(struct data_set *data,
					   void (*cb)(const struct mat *in, const struct mat *out, void *), void *user);
void data_set_print(struct data_set *data);
void data_set_run(struct data_set *data, int epochs,
				  void (*cb)(struct mat*, struct mat*));
void data_set_init(struct data_set *data, unsigned input, unsigned output);
void data_set_add(struct data_set *data, const struct mat *in, const struct mat *out);
void data_set_read(struct data_set *data, FILE *in_file);
void data_set_write(struct data_set *data, FILE *out_file);
#endif
