#ifndef MAT_H
#define MAT_H

#define MAT_OK				0
#define MAT_ALLOC_FAILED	1
#define MAT_BAD_DIMENSION	2
#define MAT_BAD_ARGUMENT	4

struct mat {
	int cols;
	int rows;
	float *vals;
	int flags;
};

struct mat* mat_init(struct mat *m, unsigned row, unsigned cols);
struct mat* mat_copy(const struct mat *m, struct mat *res);

struct mat* mat_each(struct mat *m, float (*cb)(float), struct mat *res);

struct mat* mat_set_all(struct mat *m, float val, struct mat *res);

struct mat* mat_trans(struct mat *m, struct mat *res);
struct mat* mat_mul(struct mat *a, struct mat *b, struct mat *res);
struct mat* mat_scalar(struct mat *a, float scalar, struct mat *res);

struct mat* mat_add(struct mat *a, struct mat *b, struct mat *res);
struct mat* mat_sub(struct mat *a, struct mat *b, struct mat *res);

struct mat* mat_rand(struct mat *a, float min, float max);
struct mat* mat_zero(struct mat *m);

float mat_elem(const struct mat *m, unsigned r, unsigned c);
void mat_set_elem(struct mat *m, unsigned r, unsigned c, float val);

void mat_free(struct mat *m);
void mat_print(const struct mat *m);

#endif
